import mtgtournamentorganizer.User
import mtgtournamentorganizer.Role
import mtgtournamentorganizer.UserRole

class BootStrap {
	
	def springSecurityService

    def init = { servletContext ->
		
		def adminRole = new Role(authority: 'ROLE_ADMIN').save(flush: true)
		def userRole = new Role(authority: 'ROLE_USER').save(flush: true)
		
		def testUser = new User(username: 'me', enabled: true, password: 'password', email: 'anthony.miller.csn@gmail.com', dciNumber: 'null')
		testUser.save(flush: true)
		
		UserRole.create(testUser, adminRole, true)
		
		assert User.count() == 1
		assert Role.count() == 2
		assert UserRole.count() == 1
		
    }
    def destroy = {
    }
}
