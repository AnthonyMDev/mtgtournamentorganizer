package mtgtournamentorganizer

class Event {
	
	String name
	String format
	String Location
	Date eventDate

	static hasMany = [entry: EventEntry]
	
	static scaffold = true

    static constraints = {
    }
}
